module gitlab.com/thiesen/services

go 1.13

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/elastic/go-elasticsearch/v6 v6.8.3-0.20191019103552-6a984f416d97
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mailru/easyjson v0.7.0 // indirect
	github.com/namsral/flag v1.7.4-pre // indirect
	github.com/rs/cors v1.7.0
	github.com/rs/zerolog v1.15.0 // indirect
	github.com/segmentio/kafka-go v0.3.3
	github.com/tinrab/kit v0.0.0-20190329160841-0e7c24974f7e
)
