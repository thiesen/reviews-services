FROM golang:1.13-alpine AS build
RUN apk --no-cache add gcc g++ make ca-certificates
WORKDIR /go/src/gitlab.com/thiesen/services


COPY go.mod ./go.mod
COPY go.sum ./go.sum
RUN go mod download

COPY . .

RUN go install ./cmd/reviews-consumer
RUN go install ./cmd/reviews-query

FROM alpine
WORKDIR /usr/bin
COPY --from=build /go/bin .
