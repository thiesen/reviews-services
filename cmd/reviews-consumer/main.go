package main

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/tinrab/kit/retry"
	"gitlab.com/thiesen/services/pkg/consumer"
	"gitlab.com/thiesen/services/pkg/schema"
	"gitlab.com/thiesen/services/pkg/search"
)

func initConsumer() {
	log.Println("[reviews_consumer] initializing kafka store")

	kafkaTopic := "reviews"
	clientId := "reviews-consumer-id"

	kafkaConsumer := consumer.NewKafkaConsumer(kafkaTopic, clientId, []string{"kafka:9092"})

	consumer.SetRepository(kafkaConsumer)

	log.Println("[reviews_consumer] kafka store initialized!")
}

func initSearchRepository() {
	log.Println("[reviews_consumer] initializing elasticsearch client")

	// TODO: move this logic to the elastic repo
	retry.ForeverSleep(2*time.Second, func(_ int) error {
		es, err := search.NewElastic("http://elasticsearch:9200")

		if err != nil {
			log.Println("[reviews_consumer] retrying elasticsearch connection...")

			return err
		}

		search.SetRepository(es)

		return nil
	})

	log.Println("[reviews_consumer] elasticseach initialized!")
}

func process(m []byte) error {
	log.Printf("[reviews_consumer] received message: %v", string(m))

	review := &schema.Review{}

	err := json.Unmarshal(m, review)
	if err != nil {
		log.Printf("[reviews_consumer] error unmarshalling message: %s", err.Error())

		return err
	}

	err = search.IndexReview(context.Background(), review)
	if err != nil {
		log.Printf("[reviews_consumer] error indexing review: %s", err.Error())

		return err
	}

	return nil
}

func onConsumerError(err error) error {
	log.Printf("[reviews_consumer] error processing message: %s", err.Error())

	return err // force retry on consumer loop
}

func main() {
	log.Println("[reviews_consumer] initializing consumer")

	initConsumer()
	initSearchRepository()

	defer search.Close()
	defer consumer.Close()

	log.Println("[reviews_consumer] consumer started")

	consumer.ConsumerLoop(process, onConsumerError)
}
