package main

import (
	"log"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
	"github.com/tinrab/kit/retry"
	"gitlab.com/thiesen/services/pkg/search"
)

func newRouter() http.Handler {
	router := httprouter.New()

	router.GET("/", searchHandler)

	return cors.Default().Handler(router)
}

// TODO: extract this
func initSearchRepository() {
	log.Println("[reviews_consumer] initializing elasticsearch client")

	retry.ForeverSleep(2*time.Second, func(_ int) error {
		es, err := search.NewElastic("http://elasticsearch:9200")

		if err != nil {
			log.Println("[reviews_consumer] retrying elasticsearch connection...")

			return err
		}

		search.SetRepository(es)

		return nil
	})

	log.Println("[reviews_consumer] elasticseach initialized!")
}

func main() {
	initSearchRepository()

	defer search.Close()

	router := newRouter()

	log.Fatal(http.ListenAndServe(":5000", router))
}
