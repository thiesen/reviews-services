package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/thiesen/services/pkg/search"
)

const defaultQuery = "*"

func queryValue(q string) string {
	log.Printf("received param %s", q)
	if len(q) == 0 {
		return defaultQuery
	}

	return q
}

func searchHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	urlValues := r.URL.Query()

	query := queryValue(urlValues.Get("query"))

	result, err := search.SearchReviews(r.Context(), query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(result)
}
