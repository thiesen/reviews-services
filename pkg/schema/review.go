package schema

import "time"

type Review struct {
	ID           int           `json:"id"`
	Comment      string        `json:"comment"`
	ReviewThemes []ReviewTheme `json:"themes"`
	CreatedAt    time.Time     `json:"created_at"`
}
