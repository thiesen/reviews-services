package schema

type ReviewTheme struct {
	Theme     string `json:"theme"`
	Category  string `json:"category"`
	Sentiment int8   `json:"sentiment"`
}
