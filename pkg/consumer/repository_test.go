package consumer

import (
	"testing"
)

type fakeConsumerRepository struct {
	closed           bool
	msgToBeProcessed string
	processedMessage string
}

func (f *fakeConsumerRepository) Close() {
	f.closed = true
}

func (f *fakeConsumerRepository) ConsumerLoop(onEvent func([]byte) error, onError func(error) error) {
	onEvent([]byte(f.msgToBeProcessed))
}

func TestClose(t *testing.T) {
	t.Run("delegates Close() to implementation", func(t *testing.T) {
		fakeRepo := &fakeConsumerRepository{}
		SetRepository(fakeRepo)

		Close()

		if !fakeRepo.closed {
			t.Error("search repository should have been closed")
		}
	})
}

func TestConsumerLoop(t *testing.T) {
	t.Run("delegates ConsumerLoop() with given processing function to implementation", func(t *testing.T) {
		msgToBeProcessed := "I was processed"

		fakeRepo := &fakeConsumerRepository{msgToBeProcessed: msgToBeProcessed}
		SetRepository(fakeRepo)

		ConsumerLoop(func(b []byte) error {
			fakeRepo.processedMessage = string(b) + "!!!"

			return nil
		}, nil)

		expectedMessage := "I was processed!!!"
		if fakeRepo.processedMessage != expectedMessage {
			t.Errorf("expected '%s' got '%s'", expectedMessage, fakeRepo.processedMessage)
		}
	})
}
