package consumer

import (
	"context"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/tinrab/kit/retry"
)

type KafkaConsumer struct {
	reader *kafka.Reader
}

func NewKafkaConsumer(topic, clientId string, brokers []string) *KafkaConsumer {
	config := kafka.ReaderConfig{
		Brokers:         brokers,
		GroupID:         clientId,
		Topic:           topic,
		MinBytes:        10e3,            // 10KB
		MaxBytes:        10e6,            // 10MB
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	}

	return &KafkaConsumer{reader: kafka.NewReader(config)}
}

func (k *KafkaConsumer) Close() {
	k.reader.Close()
}

func (k *KafkaConsumer) ConsumerLoop(onEvent func([]byte) error, onError func(error) error) {
	for {
		log.Println("[kafka] consumer loop started. waiting for new messages.")

		m, err := k.reader.ReadMessage(context.Background())
		if err == nil {
			retry.ForeverSleep(2*time.Second, func(_ int) error {
				log.Printf(
					"[kafka] consuming message: %s (%d) on partition %d",
					m.Key,
					m.Offset,
					m.Partition,
				)

				// run consumer processor function
				err = onEvent(m.Value)
				if err != nil {
					// if our onError function returns an error, retry processing the msg indefinitely
					// if nil is returned, process the next msg
					err := onError(err)

					return err
				} else {
					log.Printf(
						"[kafka] successfully consumed message: %s (offset %d) on partition %d",
						m.Key,
						m.Offset,
						m.Partition,
					)
				}
				return nil
			})
		} else {
			log.Printf("[kafka] error reading message: %s", err.Error())
		}
	}
}
