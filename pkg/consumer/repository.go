package consumer

type Consumer interface {
	Close()
	ConsumerLoop(func([]byte) error, func(error) error)
}

var impl Consumer

func SetRepository(es Consumer) {
	impl = es
}

func Close() {
	impl.Close()
}

func ConsumerLoop(onEvent func([]byte) error, onError func(error) error) {
	// TODO: add context?
	impl.ConsumerLoop(onEvent, onError)
}
