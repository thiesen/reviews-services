package search

import (
	"context"
	"testing"

	"gitlab.com/thiesen/services/pkg/schema"
)

type fakeSearchRepository struct {
	closed  bool
	indexed *schema.Review
	query   string
	field   string
}

func (f *fakeSearchRepository) Close() {
	f.closed = true
}

func (f *fakeSearchRepository) IndexReview(ctx context.Context, review *schema.Review) error {
	f.indexed = review

	return nil
}

func (f *fakeSearchRepository) SearchReviews(ctx context.Context, field, query string) (*SearchResults, error) {
	f.query = query
	f.field = field

	return nil, nil
}

func TestClose(t *testing.T) {
	t.Run("delegates Close() to implementation", func(t *testing.T) {
		fakeRepo := &fakeSearchRepository{}
		SetRepository(fakeRepo)

		Close()

		if !fakeRepo.closed {
			t.Error("search repository should have been closed")
		}
	})
}

func TestIndexReview(t *testing.T) {
	t.Run("delegates IndexReview() to implementation", func(t *testing.T) {
		fakeRepo := &fakeSearchRepository{}
		SetRepository(fakeRepo)

		newReview := &schema.Review{Comment: "FOO"}

		IndexReview(context.Background(), newReview)

		if fakeRepo.indexed != newReview {
			t.Errorf("wanted '%v' to be indexed. got '%v'", newReview, fakeRepo.indexed)
		}
	})
}

func TestSearchReviews(t *testing.T) {
	t.Run("delegates SearchReviews() to implementation", func(t *testing.T) {
		fakeRepo := &fakeSearchRepository{}
		SetRepository(fakeRepo)

		query := "my query"
		field := "my field"

		SearchReviews(context.Background(), field, query)

		if fakeRepo.query != query {
			t.Errorf("wanted '%s' as `query`. got '%s'", query, fakeRepo.query)
		}

		if fakeRepo.field != field {
			t.Errorf("wanted '%s' as `field`. got '%s'", field, fakeRepo.field)
		}
	})
}
