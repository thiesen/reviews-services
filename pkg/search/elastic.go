package search

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"strconv"
	"strings"

	"github.com/elastic/go-elasticsearch"
	"github.com/elastic/go-elasticsearch/esapi"
	"gitlab.com/thiesen/services/pkg/schema"
)

const indexName = "reviews"

type ElasticRepository struct {
	client *elasticsearch.Client
}

func NewElastic(url string) (*ElasticRepository, error) {
	cfg := elasticsearch.Config{
		Addresses: []string{url},
	}

	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		log.Printf("[elasticsearch] error creating new client: %s", err.Error())

		return nil, err
	}

	return &ElasticRepository{client: es}, nil
}

func (er *ElasticRepository) Close() {}

func (er *ElasticRepository) IndexReview(ctx context.Context, review *schema.Review) error {
	log.Printf("[elasticsearch] indexing review: %d", review.ID)

	js, _ := json.Marshal(review)

	req := esapi.IndexRequest{
		Index:      indexName,
		Body:       strings.NewReader(string(js)),
		DocumentID: strconv.Itoa(review.ID),
		Refresh:    "true",
	}

	res, err := req.Do(context.Background(), er.client)
	if err != nil {
		log.Printf("[elasticsearch] Error getting response: %s", err)

		return err
	}

	defer res.Body.Close()

	if res.IsError() {
		buf := new(bytes.Buffer)

		buf.ReadFrom(res.Body)
		errorResponse := buf.String()

		log.Printf(
			"[elasticsearch][%s] Error indexing document ID=%d: %s",
			res.Status(),
			review.ID,
			errorResponse,
		)
	} else {
		var r map[string]interface{}

		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("[elasticsearch] Error parsing the response body: %s", err)
		} else {
			// Print the response status and indexed document version.
			log.Printf(
				"[elasticsearch][%s] successfully indexed review: %d! %s version=%d",
				res.Status(),
				review.ID,
				r["result"],
				int(r["_version"].(float64)),
			)
		}
	}
	return nil
}

func (er *ElasticRepository) SearchReviews(ctx context.Context, query string) (*SearchResults, error) {
	queryBuffer := newQueryBuffer(query)

	res, err := er.client.Search(
		er.client.Search.WithContext(ctx),
		er.client.Search.WithIndex(indexName),
		er.client.Search.WithBody(queryBuffer),
		er.client.Search.WithTrackTotalHits(true),
		er.client.Search.WithPretty(),
	)

	if err != nil {
		log.Printf("Error getting response: %s", err)

		return nil, err
	}

	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Printf("Error parsing the response body: %s", err)

			return nil, err
		} else {
			// Print the response status and error information.
			log.Printf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	parsedResponse, err := parseSearchResponse(res)

	return &SearchResults{
		Count:      parsedResponse.getCount(),
		Categories: parsedResponse.getCategories(),
		Themes:     parsedResponse.getThemes(),
		Reviews:    parsedResponse.getReviews(),
	}, nil
}
