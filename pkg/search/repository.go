package search

import (
	"context"

	"gitlab.com/thiesen/services/pkg/schema"
)

type AverageSentiment struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

type SearchResults struct {
	Count      int                 `json:"count"`
	Categories []*AverageSentiment `json:"categories"`
	Themes     []*AverageSentiment `json:"themes"`
	Reviews    []*schema.Review    `json:"reviews"`
}

type Repository interface {
	Close()

	IndexReview(ctx context.Context, review *schema.Review) error
	SearchReviews(ctx context.Context, query string) (*SearchResults, error)
}

var impl Repository

func SetRepository(repository Repository) {
	impl = repository
}

func Close() {
	impl.Close()
}

func IndexReview(ctx context.Context, review *schema.Review) error {
	return impl.IndexReview(ctx, review)
}

func SearchReviews(ctx context.Context, query string) (*SearchResults, error) {
	return impl.SearchReviews(ctx, query)
}
