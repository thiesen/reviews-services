package search

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"

	"github.com/elastic/go-elasticsearch/esapi"
	"gitlab.com/thiesen/services/pkg/schema"
)

type esResponse struct {
	Hits struct {
		Total int `json:"total"`
		Hits  []struct {
			Source *schema.Review `json:"_source"`
		} `json:"hits"`
	} `json:"hits"`
	Aggregations struct {
		AvgByCategory struct {
			Buckets []struct {
				Key          string `json:"key"`
				AvgSentiment struct {
					Value float64 `json:"value"`
				} `json:"avg_sentiment"`
			} `json:"buckets"`
		} `json:"avg_by_category"`
		AvgByTheme struct {
			Buckets []struct {
				Key          string `json:"key"`
				AvgSentiment struct {
					Value float64 `json:"value"`
				} `json:"avg_sentiment"`
			} `json:"buckets"`
		} `json:"avg_by_theme"`
	} `json:"aggregations"`
}

func parseSearchResponse(r *esapi.Response) (*esResponse, error) {
	response := &esResponse{}

	if err := json.NewDecoder(r.Body).Decode(&response); err != nil {
		log.Printf("Error parsing the response body: %s", err)

		return nil, err
	}

	return response, nil
}

func (esr *esResponse) getCategories() []*AverageSentiment {
	var categories []*AverageSentiment
	for _, bucket := range esr.Aggregations.AvgByCategory.Buckets {
		averageSentiment := &AverageSentiment{
			Name:  bucket.Key,
			Value: bucket.AvgSentiment.Value,
		}

		categories = append(categories, averageSentiment)
	}

	return categories
}

func (esr *esResponse) getThemes() []*AverageSentiment {
	var themes []*AverageSentiment
	for _, bucket := range esr.Aggregations.AvgByTheme.Buckets {
		averageSentiment := &AverageSentiment{
			Name:  bucket.Key,
			Value: bucket.AvgSentiment.Value,
		}

		themes = append(themes, averageSentiment)
	}

	return themes
}

func (esr *esResponse) getReviews() []*schema.Review {
	var searchResults []*schema.Review

	for _, hit := range esr.Hits.Hits {
		searchResults = append(searchResults, hit.Source)
	}

	return searchResults
}

func (esr *esResponse) getCount() int {
	return esr.Hits.Total
}

func newQueryBuffer(query string) *bytes.Buffer {
	esQuery := fmt.Sprintf(
		`{
	     "query": {
         "query_string": {
           "query": "%s",
             "default_field": "comment"
           }
         },
       "aggregations":{
         "avg_by_category":{
           "terms":{
             "field":"themes.category.keyword",
             "order":{
               "avg_sentiment":"desc"
             }
           },
           "aggs":{
             "avg_sentiment":{
               "avg":{
                 "field":"themes.sentiment"
               }
             }
           }
         },
         "avg_by_theme":{
           "terms":{
             "field":"themes.theme.keyword",
             "order":{
               "avg_sentiment":"desc"
             }
           },
           "aggs":{
             "avg_sentiment":{
               "avg":{
                 "field":"themes.sentiment"
               }
             }
           }
         }
       }
     }
	`,
		query,
	)

	return bytes.NewBuffer([]byte(esQuery))
}
